import * as bodyParser from 'body-parser';
import * as express from 'express';
import {Request} from 'express';


const app = express();

app.use(bodyParser.json());

const API_URL = '/kalkulacka-api';

app.post(`${API_URL}/calc`, (req: Request, res: any) => {
  let total = req.body.total;
  let months = req.body.months;
  let monthly = Math.round(total * ((Math.pow(1.0125,months)*(1.0125-1))/(Math.pow(1.0125,60)-1)));
  if(req.body.insurance == 1){
    monthly+=100;
  }
  res.send({loanValue: monthly});
});


app.listen(3100, () => {
  console.log('Express is listening to http://localhost:3100');
});
