
import React from 'react';
import './Calc.css';
import SliderComponent from '../Slider/SliderComponent';
import Radios from '../Radios/Radios';
import Loan from '../Loan/Loan'
import {connect} from 'react-redux';
import {getLoanCost, getLoanUI, getRadio, getSliders} from "../../utils/selectors";
import {changeLoader, changeLoanValue, changeRadio} from "../../actions";
 class Calc extends React.Component {



    radioChange = (e) => {
        this.props.changeRadio({value: e.target.value});
        this.props.changeLoanCost({total: this.props.sliders[0].value, months: this.props.sliders[1].value, insurance: e.target.value});
    };
    sliderAfterChange = (value) => {
        this.props.changeLoanCost({total: this.props.sliders[0].value, months: this.props.sliders[1].value, insurance: this.props.radio.value});
    }

    render() {
        return (
            <div className="calc-component">
                <div className="left-column">

                    <h1>Expres půjčku schválíme do 5 minut</h1>
                    {
                        this.props.sliders.map(slider => {
                            return (<SliderComponent  id={slider.id} min={slider.min} max={slider.max} onAfterChange={this.sliderAfterChange}
                                                     step={slider.step} key={slider.id}
                                                     header={slider.header} unit={slider.unit}
                                                     leftLable={slider.leftLabel} rightLabel={slider.rightLabel}/>);
                        })
                    }
                    <Radios header={this.props.radio.header} radios={this.props.radio.radios}
                            name={this.props.radio.name} value={this.props.radio.value} onChange={this.radioChange}/>
                </div>
                <div className="right-column">

                    <Loan header={this.props.loan.heading} number={this.props.loanCost} loading={this.props.loan.loader}/>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
     return {
         sliders: getSliders(state),
         radio: getRadio(state),
         loan: getLoanUI(state),
         loanCost: getLoanCost(state)
     }
}
const mapDispatchToProps = (dispatch) => {
    return  {
        changeRadio: radio => dispatch(changeRadio(radio)),
        changeLoanCost: loan => dispatch(changeLoanValue(loan)),
        changeLoader: (loader) => dispatch(changeLoader(loader))
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Calc)
