import React, {Component} from 'react';
import './SliderComponent.css';
import 'rc-slider/assets/index.css';
import Slider from 'rc-slider';
import {getSliderInputValue, getSliderValue} from "../../utils/selectors";
import {connect} from "react-redux";
import {changeSlider} from "../../actions";

class SliderComponent extends Component {


    onChange = (value) => {
        this.props.changeSlider({id: this.props.id, value: value});
        this.props.changeSlider({id: this.props.id, inputValue:value});
    };
    inputChange = (e) => {

        this.props.changeSlider({id: this.props.id, inputValue: e.target.value});

    };

    inputBlur = (e) => {
        let value = e.target.value;
        if(value > this.props.max){
            value = this.props.max;
        }
        if(value < this.props.min){
            value = this.props.min;
        }
        this.onChange(value);
        this.props.onAfterChange(value);
    };
    onInputKeyPress = (e) => {
        const keyCode = e.keyCode || e.which;
        const keyValue = String.fromCharCode(keyCode);
        if (/[^0-9]/g.test(keyValue))
            e.preventDefault();
    };
    onAfterChange = (value) =>
        this.props.onAfterChange(value);

    render() {
        const settings = {
            min: this.props.min ? this.props.min : 0,
            max: this.props.max ? this.props.max : 1000,
            value: this.props.value ? this.props.value : 0,
            step: this.props.step ? this.props.step : 1
        };

        const railStyle =
            {backgroundColor: '#F2F2F2', height: 10};
        const trackStyle = {backgroundColor: '#5ABE76', height: 10};
        const handleStyle = {
            borderColor: '#EAEAEA',
            height: 28,
            borderWidth: 1,
            width: 50,
            marginLeft: -25,
            borderRadius: 14,
            marginTop: -9,
            backgroundColor: '#fdfdfd',
        };
        return (
            <div className="slider-component">
                <h2>{this.props.header ? this.props.header : ' '}</h2>
                <div className="slider-wrapper">
                    <div className="slider"><Slider onChange={this.onChange} onAfterChange={this.onAfterChange}
                                                    min={settings.min} max={settings.max}
                                                    defaultValue={Number(settings.value)}
                                                    step={settings.step} value={Number(settings.value)}
                                                    railStyle={railStyle} handleStyle={handleStyle}
                                                    trackStyle={trackStyle}/>
                        <div className="labels">
                            <span
                                className="left-label">{this.props.leftLabel ? this.props.leftLabel : this.props.min + ' ' + this.props.unit}</span>
                            <span
                                className="right-label">{this.props.rightLabel ? this.props.rightLabel : this.props.max + ' ' + this.props.unit}</span>
                        </div>
                    </div>
                    <div className="input">
                        <input value={Number(this.props.inputValue)} onBlur={this.inputBlur} onChange={this.inputChange} onKeyPress={this.onInputKeyPress}
                        /><span className="unit">{this.props.unit ? this.props.unit : ' '}</span></div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        value: getSliderValue(state, ownProps.id),
        inputValue: getSliderInputValue(state, ownProps.id)
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        changeSlider: slider => dispatch(changeSlider(slider))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SliderComponent)
