import React from 'react';
import './Loan.css';



const Loan = ({header, number, loading}) => {
    return (<div className={"loan "}>
            <div className={"loanOverlay "+(loading ? 'show': '')}>
                <div className="lds-roller">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
            <h2>{header}</h2>
            <span className="big-number">{number} Kč</span>

        </div>
    );
};
export default Loan;

