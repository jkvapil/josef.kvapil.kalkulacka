import React from 'react';
import './Radios.css';


const Radios = (props) => {
    const inputs = props.radios.map(radio => {
        return <label key={radio.id}><input type="radio" name={props.name} value={radio.value} onChange={props.onChange} key={radio.id} checked={radio.value===Number(props.value)}/>{radio.label}</label>
    });
    return (<div className="radios">
            <h2>{props.header}</h2>
            {inputs}
        </div>
    );
};
export default Radios;
