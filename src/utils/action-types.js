export const ActionType = {
    CHANGE_SLIDER_VALUE: "CHANGE_SLIDER_VALUE",
    CHANGE_RADIO_VALUE: "CHANGE_RADIO_VALUE",
    CHANGE_LOAN_COST: "CHANGE_LOAN_COST",
    CHANGE_LOAN_LOADER: "CHANGE_LOAN_LOADER"
};