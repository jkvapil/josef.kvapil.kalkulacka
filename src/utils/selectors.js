export function getSliders(state) {
    return state.ui.sliders;
}

export function getRadio(state) {
    return state.ui.radio;
}

export function getSliderValue(state, id){

    return state.ui.sliders.find(slider => {
        return slider.id === id
    }).value;
}

export function getSliderInputValue(state, id){

    return state.ui.sliders.find(slider => {
        return slider.id === id
    }).inputValue;
}
export function getLoanCost(state){
    return state.entity.loan.cost;
}
export function getLoanUI(state){
    return state.ui.loan;
}