import {ActionType} from "../utils/action-types";
import axios from "axios";

export function changeSlider(payload) {
    return { type: ActionType.CHANGE_SLIDER_VALUE, payload }
};


export function changeLoanValue(payload){
    return (dispatch) => {
        dispatch(changeLoader({loader: true}));
        axios.post(`kalkulacka-api/calc`, {
            total: payload.total,
            months: payload.months,
            insurance: payload.insurance
        })
            .then(response => {
                dispatch(getLoanValueSuccess(response.data.loanValue));
                dispatch(changeLoader({loader: false}))

            })
            .catch(error => {
                throw(error);
            });
    }
}


export const getLoanValueSuccess =  (cost) => {
    return {
        type: ActionType.CHANGE_LOAN_COST,
        payload: {
            cost: cost
        }
    }
};
export function changeRadio(payload) {
    return {type: ActionType.CHANGE_RADIO_VALUE, payload}
};

export function changeLoader(payload){
    return {type: ActionType.CHANGE_LOAN_LOADER, payload}
}