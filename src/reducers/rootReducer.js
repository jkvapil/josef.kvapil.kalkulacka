import sliderReducer from "./sliderReducer";
import radioReducer from "./radioReducer";
import {ActionType} from "../utils/action-types";
import loanReducer from "./loanReducer";

const initState =
    {
        ui: {
            sliders: [
                {id: 2, min: 20000, max: 800000, step: 1000, value: 100000, header: 'Kolik si chci půjčit', unit: 'Kč', inputValue: 100000},
                {
                    id: 1,
                    min: 24,
                    max: 96,
                    step: 1,
                    value: 48,
                    header: 'Na jak dlouho',
                    unit: 'Měsíců',
                    leftLabel: '24 měsíců',
                    rightLabel: '96 měsíců',
                    inputValue: 48
                },

            ],
            radio: {
                header: "S pojištěním proti neschopnosti půjčku splácet",
                name: "pojisteni",
                value: 0,
                radios: [
                    {label: "S pojištěním", value: 1, id: 1},
                    {label: "Bez pojištění", value: 0, id: 2}
                ]
            },
            loan: {
                heading: "Měsíčně zaplatíte",
                loader:false

            }

        },
        entity: {
            loan: {cost: "2050"}
        }


    };


const rootReducer = (state = initState, action) => {
    switch (action.type) {
        case ActionType.CHANGE_SLIDER_VALUE:
            return sliderReducer(state, action);
        case ActionType.CHANGE_RADIO_VALUE:
            return radioReducer(state, action);
        case ActionType.CHANGE_LOAN_COST:
        case ActionType.CHANGE_LOAN_LOADER:
            return loanReducer(state, action);
        default:
            return state;
    }

};
export default rootReducer;


