import {ActionType} from "../utils/action-types";


const radioReducer = (state, action) => {
    switch (action.type) {
        case ActionType.CHANGE_RADIO_VALUE:
            return radioValueReducer(state,action);
        default:   return state

    }

};

const radioValueReducer = (state, action) => {

    return {...state, ui: {...state.ui, radio: {...state.ui.radio, value: action.payload.value}}};
};

export default radioReducer;

