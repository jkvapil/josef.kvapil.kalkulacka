import {ActionType} from "../utils/action-types";


const loanReducer = (state, action) => {
    switch (action.type) {
        case ActionType.CHANGE_LOAN_COST:
            return loanCostReducer(state,action);
        case ActionType.CHANGE_LOAN_LOADER:
            return loaderReducer(state,action);
        default:   return state

    }

};
const loaderReducer = (state, action) => {

    return {...state, ui: {...state.ui, loan: {...state.ui.loan, loader: action.payload.loader}}};
};

const loanCostReducer = (state, action) => {

    return {...state, entity: {...state.entity, loan: {...state.entity.loan, cost: action.payload.cost}}};
};

export default loanReducer;

