import {ActionType} from "../utils/action-types";

const sliderReducer = (state, action) => {
    switch (action.type) {
        case ActionType.CHANGE_SLIDER_VALUE:
            return sliderValueReducer(state,action);
        default:   return state

    }

};

const sliderValueReducer = (state, action) => {
     let slider = state.ui.sliders.find((slider) => {
        return slider.id === action.payload.id
    });
   const  index = state.ui.sliders.findIndex((slider) => {
       return slider.id === action.payload.id
   });


    if(action.payload.value>=0){
     slider.value = action.payload.value;
    return {...state, ui: {...state.ui, sliders: Object.assign([...state.ui.sliders], {[index]: slider})}};
    }else if( action.payload.inputValue>=0){
        slider.inputValue = action.payload.inputValue;
        return {...state, ui: {...state.ui, sliders: Object.assign([...state.ui.sliders], {[index]: slider})}};
    }else{


    return state;}
};

export default sliderReducer;

